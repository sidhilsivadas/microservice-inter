<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository
{

    public static function saveUser($formData){
    	$user = new User;
    	$user->first_name = $formData->firstName;
        $user->last_name = $formData->lastName;
        $user->email = $formData->email;
        $user->phone_number = $formData->phoneNumber;
        $user->address = $formData->address;
        $user->bank_account_details = json_encode($formData->bankAccountDetails);
        //dd($user);
        //$todo->status = $formData["data"]["status"];
        $user->save();
    }


    public static function updateUser($formData,$id){
    	$user = User::find($id);
    	$user->first_name = $formData->firstName;
        $user->last_name = $formData->lastName;
        $user->email = $formData->email;
        $user->phone_number = $formData->phoneNumber;
        $user->address = $formData->address;
        $user->bank_account_details = json_encode($formData->bankAccountDetails);
        $user->save();
    }

    public static function getUser($id){
    	$todo = User::where('id', $id)->first();
    	return $todo;
    }

    public static function getallUsers(){
        $todo = User::all();
        return $todo;
    }


    public static function deleteUser($id){
        User::find($id)->delete();
    }


    

    
}
